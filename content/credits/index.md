---
title: Credits
weight: 10
---

The main developing team of APE is composed of:

- Micael Oliveira
- Fernando Nogueira

Other people who made significant contributions to the code:

- Tiago Cerqueira
- Pedro Borlido
