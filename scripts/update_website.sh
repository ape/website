#!/bin/bash

GITDIR="$1"
WEBSITEDIR="$2"
NEWTAG="$3"


### Sanity checks ###

if [ ! -d "${GITDIR}/.git" ]; then
    echo "Error: ${GITDIR}/.git directory does not exist."
    exit 1
fi

if [ ! -d "${WEBSITEDIR}" ]; then
    echo "Error: ${WEBSITEDIR} directory does not exist."
    exit 1
fi


### Information about tags ###

# Get a list of known tags from the git repository
tags=$(git --git-dir ${GITDIR}/.git tag)


# Check is the new tag is the latest version
is_latest=true
for tag in $tags; do
    IFS=.
    new=($NEWTAG)
    old=($tag)

    for ((i=${#old[@]}; i<${#new[@]}; i++)); do
        old[i]=0
    done
    for ((i=0; i<${#old[@]}; i++)); do
        if [[ -z ${new[i]} ]]; then
            new[i]=0
        fi
        if ((10#${new[i]} < 10#${old[i]})); then
            is_latest=false
	    break
        fi	
	if ((10#${new[i]} > 10#${old[i]})); then
	    break
        fi
    done

    unset IFS
done


### Version ###
if [ "$is_latest" = true ] ; then
    sed -i -E "s/version = \"*.*.*\"/version = \"$NEWTAG\"/" ${WEBSITEDIR}/config.toml
fi


### Documentation ###
if [ "$is_latest" = true ] ; then

    # Installation
    cat > ${WEBSITEDIR}/content/installation/index.md << EOF
---
title: Installation
weight: 10
---

EOF
    cat ${GITDIR}/doc/installation.md >> ${WEBSITEDIR}/content/installation/index.md

    # Index
    cat > ${WEBSITEDIR}/content/input/index.md << EOF
---
title: Input
weight: 10
---

EOF
    cat ${GITDIR}/doc/input.md >> ${WEBSITEDIR}/content/input/index.md

    # Examples
    cat > ${WEBSITEDIR}/content/examples/index.md << EOF
---
title: Examples
weight: 10
---

EOF
    cat ${GITDIR}/doc/examples.md >> ${WEBSITEDIR}/content/examples/index.md
fi
